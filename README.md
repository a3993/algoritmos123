# *Algoritmos* 

![Imagen de algoritmo](https://blog.pandorafms.org/wp-content/uploads/2018/05/que-es-un-algoritmo-featured.png)

## **Que es un Algoritmo?**

Un algoritmo es un conjunto de operaciones simples a través de las cuales obtenemos una solución a un problema 

## **Características**

* Tienen un inicio y un final
* Cada instrucción es única y obedece un objetivo
* Funciona en secuencia
* Si sigue un algoritmo 2 veces tiene que dar el mismo resultado en las dos

## **Partes de un Algoritmo**

* **Input**

_La información que damos al algoritmo con la que va a trabajar para ofrecer una solución al problema._

* **Proceso**

_Conjunto de pasos para llegar a la solución del problema._

* **Output**

_Resultado final del problema._

## **Ejemplos**

|ejemplos|
|---------|
|recetas de cocina|
|manuales|
|cálculos matemáticos|




